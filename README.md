#### Description : VPC template without an IGW

#### Author : Patrick Hynes

#### Date: 28.11.17

### Repository Structure

 * [vpc.yaml](vpc.yaml)
 * [README.md](README.md)



### Cloudformation Resources - IAM Roles

* VPC
* Private Internal Subnet 1 in AZ eu-central-1a
* Private Internal Subnet 2 in AZ eu-central-1b
* Private Internal Subnet 3 in AZ eu-central-1c
* Route Table





